<?php


use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class InstitutionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('institutions')->delete();
        $institutions = [
            ['name' => 'Home1', 'address' => 'home1', 'email'=> 'institute1@gmail.com','phone'=>'12345671','mobile'=>'+9212345671','fax'=>'+13341','contact_person'=>'Ali1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Home2', 'address' => 'home2', 'email'=> 'institute2@gmail.com','phone'=>'12345672','mobile'=>'+9212345672','fax'=>'+13342','contact_person'=>'Ali2','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Home3', 'address' => 'home3', 'email'=> 'institute3@gmail.com','phone'=>'12345673','mobile'=>'+9212345673','fax'=>'+13343','contact_person'=>'Ali3','created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ];
        // make sure you do the insert
        DB::table('institutions')->insert($institutions);
    }
}
