<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Course;

class CourseUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('course_user')->delete();
        $user_id = User::where('email', 'teacher@sms.com')->value('id');
        $course_id= Course::where('code','1011E-1')->value('id');
        $data = [
            ['user_id' => $user_id, 'course_id' => $course_id]
        ];
        // make sure you do the insert
        DB::table('course_user')->insert($data);
    }
}

