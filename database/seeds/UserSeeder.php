<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        $users = [
            ['name' => 'Principal', 'email' => 'principal@sms.com', 'password' => Hash::make('principal_1234'), 'active'=>true, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Teacher', 'email' => 'teacher@sms.com', 'password' => Hash::make('teacher_1234'), 'active'=>true, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Student', 'email' => 'student@sms.com', 'password' =>  Hash::make('student_1234'), 'active'=>true, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ];
        // make sure you do the insert
        DB::table('users')->insert($users);
    }
}
