<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Role;

class SalarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('salaries')->delete();
        $user_id = User::where('email', 'principal@sms.com')->value('id');
        $role_id = Role::where('name', 'principal')->value('id');
        $data = [
            ['user_id' => $user_id, 'role_id' => $role_id , 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ];
        // make sure you do the insert
        DB::table('salaries')->insert($data);
    }
}
