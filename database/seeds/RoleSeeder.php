<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Role;
use Carbon\Carbon;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('roles')->delete();

        $roles = [
            ['name' => 'principal', 'label' => 'Principal', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'teacher', 'label' => 'Teacher', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'student', 'label' => 'Student', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ];
        // make sure you do the insert
        DB::table('roles')->insert($roles);

    }
}
