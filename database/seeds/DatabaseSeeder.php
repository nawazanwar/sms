<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(ProfileSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(RoleUserSeeder::class);
        $this->call(PermissionRoleSeeder::class);
        $this->call(GradeSeeder::class);
        $this->call(AcademicSeeder::class);
        $this->call(SlideSeeder::class);
        $this->call(NavSeeder::class);
        $this->call(InstitutionSeeder::class);
        $this->call(InstitutionBranchesSeeder::class);
        $this->call(CourseSeeder::class);
        $this->call(CourseUserSeeder::class);
        $this->call(SalarySeeder::class);
        $this->call(CourseGradeSeeder::class);

    }
}
