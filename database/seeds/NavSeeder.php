<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class NavSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('navs')->delete();
        $navs = [
            ['name' => 'Home', 'url' => 'home', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Projects', 'url' => 'projects', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'About Us', 'url' => 'about', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Contact Us', 'url' => 'contact', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ];
        // make sure you do the insert
        DB::table('navs')->insert($navs);
    }
}
