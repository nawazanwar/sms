<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class InstitutionBranchesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('institutionbranches')->delete();
        $temp_data = [
            ['location' => 'faisalabad','created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ];
        // make sure you do the insert
        DB::table('institutionbranches')->insert($temp_data);
    }
}
