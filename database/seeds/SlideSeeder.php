<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SlideSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slides')->delete();
        $slides = [
            ['first_heading' => 'first1', 'second_heading' => 'second1', 'image'=>'1.jpg' , 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['first_heading' => 'first2', 'second_heading' => 'second2', 'image'=>'2.jpg' , 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['first_heading' => 'first3', 'second_heading' => 'second3', 'image'=>'3.jpg' , 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['first_heading' => 'first4', 'second_heading' => 'second4', 'image'=>'4.jpg' , 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['first_heading' => 'first5', 'second_heading' => 'second5', 'image'=>'5.jpg' , 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ];
        // make sure you do the insert
        DB::table('slides')->insert($slides);
    }
}
