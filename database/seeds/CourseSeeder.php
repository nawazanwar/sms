<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Grade;


class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->delete();
        $grade_id = Grade::where('name', 'class9(pre-science)')->value('id');
        $slides = [
            ['grade_id' => $grade_id, 'name' => 'physics-I', 'description' => 'physics description', 'code' => '1011E-1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['grade_id' => $grade_id, 'name' => 'physics-II', 'description' => 'physics description', 'code' => '1011E-2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['grade_id' => $grade_id, 'name' => 'physics-III', 'description' => 'physics description', 'code' => '1011E-3', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ];
        // make sure you do the insert
        DB::table('courses')->insert($slides);
    }
}
