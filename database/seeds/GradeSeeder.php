<?php

use Illuminate\Database\Seeder;
use App\Models\Grade;
use Carbon\Carbon;

class GradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grades')->delete();
        $grades = [
            ['name' => 'Playgroup', 'label' => 'Playgroup', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Nursery', 'label' => 'Nursery', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Kindergarten', 'label' => 'Kindergarten', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'class1', 'label' => 'Class1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'class2', 'label' => 'Class2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'class3', 'label' => 'Class3', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'class4', 'label' => 'Class4', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'class5', 'label' => 'Class5', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'class6', 'label' => 'Class6', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'class7', 'label' => 'Class7', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'class8', 'label' => 'Class8', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'class9(pre-science)', 'label' => 'Class 9 Pre Science', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'class9(pre-arts)', 'label' => 'Class 9 Pre Arts', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'matric(pre-science)', 'label' => 'Matric  Pre Science', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'matric(pre-arts)', 'label' => 'Matric 9 Pre Arts', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ];
        // make sure you do the insert
        DB::table('grades')->insert($grades);
    }
}
