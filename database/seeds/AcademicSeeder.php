<?php

use Illuminate\Database\Seeder;

use App\Models\Academic;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AcademicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('academics')->delete();
        $academics = [
            ['start' => Carbon::now(), 'end' => Carbon::now(), 'active'=>1 , 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ];
        // make sure you do the insert
        DB::table('academics')->insert($academics);
    }
}
