<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Course;
use App\Models\Grade;

class CourseGradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('course_grade')->delete();
        $data = [
            [
                'course_id' => Course::where('code', '1011E-1')->value('id'),
                'grade_id' => Grade::where('name', 'Playgroup')->value('id') ,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];
        // make sure you do the insert
        DB::table('course_grade')->insert($data);
    }
}
