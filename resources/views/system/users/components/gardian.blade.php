<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-12">
                <h3 class="text-center">Father Detail</h3>
            </div>
            <div class="col-sm-6">
                <label>Name </label>
                <input class="form-control form-control-sm" name="Parentdetails[father_name]"
                       id="Parentdetails_father_name" type="text"
                       maxlength="60">
            </div>
            <div class="col-sm-6">
                <label>Mobile</label>
                <input class="form-control form-control-sm" name="Parentdetails[father_mobile]"
                       id="Parentdetails_father_mobile" type="text"
                       maxlength="60">
            </div>
            <div class="col-sm-6">
                <label>Job</label>
                <input class="form-control form-control-sm" name="Parentdetails[father_job]"
                       id="Parentdetails_father_job" type="text"
                       maxlength="60">
            </div>
            <div class="col-sm-6">
                <label>Aaadhar number</label>
                <input class="form-control form-control-sm" name="Parentdetails[faadharno]" id="Parentdetails_faadharno"
                       type="text"
                       maxlength="12">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-12">
                <h3 class="text-center">Mother Detail</h3>
            </div>
            <div class="col-sm-6">
                <label>Name</label>
                <input class="form-control form-control-sm" name="Parentdetails[mother_name]"
                       id="Parentdetails_mother_name" type="text"
                       maxlength="60">
                <div class="school_val_error" id="Parentdetails_mother_name_em_" style="display:none"></div>
            </div>
            <div class="col-sm-6">
                <label for="reg_input_name">Mobile</label>
                <input class="form-control form-control-sm" name="Parentdetails[mother_mobile]"
                       id="Parentdetails_mother_mobile" type="text"
                       maxlength="60">
                <div class="school_val_error" id="Parentdetails_mother_mobile_em_" style="display:none"></div>
            </div>
            <div class="col-sm-6">
                <label for="reg_input_name">Job</label>
                <input class="form-control form-control-sm" name="Parentdetails[mother_job]"
                       id="Parentdetails_mother_job" type="text"
                       maxlength="60">
                <div class="school_val_error" id="Parentdetails_mother_job_em_" style="display:none"></div>
            </div>
            <div class="col-sm-6">
                <label>Aaadhar number</label>
                <input class="form-control form-control-sm" name="Parentdetails[maadharno]" id="Parentdetails_maadharno"
                       type="text"
                       maxlength="12">
                <div class="school_val_error" id="Parentdetails_maadharno_em_" style="display:none"></div>
            </div>
        </div>
    </div>
</div>
