@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-body">
                    @include('partials.dashboard.message')
                    {!! Form::open(['route' => 'system.settings.update', 'files' => true] ) !!}
                    {!! csrf_field() !!}
                    <div class="card">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link active" href="#general" data-toggle="tab">General</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="#sliders" data-toggle="tab">Sliders</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="#socials" data-toggle="tab">Social</a>
                                </li>
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="general">
                                    General
                                </div>
                                <div class="tab-pane" id="sliders">
                                    Sliders
                                </div>
                                <div class="tab-pane" id="socials">
                                    Socials
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
