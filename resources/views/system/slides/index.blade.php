@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-body">
                    @include('partials.dashboard.message')
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">First Heading</th>
                            <th class="text-center">Second Heading</th>
                            <th class="text-center">Created At</th>
                            <th class="text-center">Last Modified</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $d)
                            <tr>
                                <td class="text-center">{{$d->first_heading}}</td>
                                <td class="text-center">{{$d->second_heading}}</td>
                                <td class="text-center">{{$d->created_at}}</td>
                                <td class="text-center">{{$d->updated_at->diffForHumans()}}</td>
                                <td class="text-center">
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix">
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>
@stop
