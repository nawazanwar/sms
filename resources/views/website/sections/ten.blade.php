<section class="bil-why-choose-us">
    <div class="auto-container">
        <div class="row">
            <div class="content-column col-lg-6 col-md-12 col-sm-12 order-2">
                <div class="inner-column">
                    <div class="sec-title">
                        <span class="title">JOIN THE EVENT</span>
                        <h2>Why Choose Eventrox?</h2>
                        <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                            eiusmtempor incididunt labore et dolore magna aliqu enim ad minim veniam quis nostrud
                            exercitation ullamco laboris nisi ut aliquip
                        </div>
                    </div>
                    <ul class="list-style-one">
                        <li>High Quality Education</li>
                        <li>You can learn anything</li>
                        <li>We list your options by state</li>
                        <li>Expert-created content and resources</li>
                    </ul>
                    <div class="btn-box">
                        <a href="buy-ticket.html" class="theme-btn btn-style-two"><span class="btn-title">Get Tickets</span></a>
                    </div>
                </div>
            </div>
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
                <div class="image-box">
                    <figure class="image"><img src="/website/images/background/3.jpg" alt=""></figure>
                </div>
            </div>
        </div>
    </div>
</section>
