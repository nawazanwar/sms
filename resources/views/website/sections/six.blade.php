<section class="bil-fun-fact-section">
    <div class="auto-container">
        <div class="fact-counter">
            <div class="row clearfix">

                <!--Column-->
                <div class="counter-column col-lg-3 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="count-box">
                        <span class="icon icon_headphones"></span>
                        <span class="count-text" data-speed="3000" data-stop="190">0</span>
                        <h4 class="counter-title">Participants</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="400ms">
                    <div class="count-box">
                        <span class="icon icon_ribbon_alt"></span>
                        <span class="count-text" data-speed="3000" data-stop="62">0</span>
                        <h4 class="counter-title">Awards Win</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="800ms">
                    <div class="count-box">
                        <span class="icon icon_like"></span>
                        <span class="count-text" data-speed="3000" data-stop="54">0</span>
                        <h4 class="counter-title">Certified Teachers</h4>
                    </div>
                </div>

                <!--Column-->
                <div class="counter-column col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="count-box">
                        <span class="icon icon_book_alt"></span>
                        <span class="count-text" data-speed="3000" data-stop="38">0</span>
                        <h4 class="counter-title">Courses</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
