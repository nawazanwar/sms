<section class="coming-soon-section">
    <div class="auto-container">
        <div class="outer-box">
            <div class="time-counter">
                <div class="time-countdown clearfix" data-countdown="12/1/2019"></div>
            </div>
        </div>
    </div>
</section>
