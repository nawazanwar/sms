<section class="bill-speakers-section" style="background-image: url({{ asset('/website/images/background/6.jpg') }});">
    <div class="auto-container">
        <div class="sec-title light text-center">
            <span class="title">Speakers</span>
            <h2>Todays Performers</h2>
        </div>

        <div class="row">
            <!-- Speaker Block -->
            <div class="speaker-block col-lg-3 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <figure class="image"><a href="speakers-detail.html"><img
                                    src="{{ asset("website/images/resource/speaker-1.jpg") }}" alt=""></a></figure>
                    </div>
                    <div class="info-box">
                        <div class="inner">
                            <h4 class="name"><a href="speakers-detail.html">Dale Marke</a></h4>
                            <span class="designation">Event Manager</span>
                            <ul class="social-links social-icon-colored">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Speaker Block -->
            <div class="speaker-block col-lg-3 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <figure class="image"><a href="speakers-detail.html"><img
                                    src="{{ asset("website/images/resource/speaker-2.jpg") }}" alt=""></a></figure>
                    </div>
                    <div class="info-box">
                        <div class="inner">
                            <h4 class="name"><a href="speakers-detail.html">Natisha Decoux</a></h4>
                            <span class="designation">Event Manager</span>
                            <ul class="social-links social-icon-colored">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Speaker Block -->
            <div class="speaker-block col-lg-3 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <figure class="image"><a href="speakers-detail.html"><img
                                    src="{{ asset("website/images/resource/speaker-3.jpg") }}" alt=""></a></figure>
                    </div>
                    <div class="info-box">
                        <div class="inner">
                            <h4 class="name"><a href="speakers-detail.html">Adolfo Plahs</a></h4>
                            <span class="designation">Event Manager</span>
                            <ul class="social-links social-icon-colored">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Speaker Block -->
            <div class="speaker-block col-lg-3 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <figure class="image"><a href="speakers-detail.html"><img
                                    src="{{ asset("website/images/resource/speaker-4.jpg") }}" alt=""></a></figure>
                    </div>
                    <div class="info-box">
                        <div class="inner">
                            <h4 class="name"><a href="speakers-detail.html">Mitchell Heggestad</a></h4>
                            <span class="designation">Event Manager</span>
                            <ul class="social-links social-icon-colored">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Speaker Block -->
            <div class="speaker-block col-lg-3 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <figure class="image"><a href="speakers-detail.html"><img
                                    src="{{ asset("website/images/resource/speaker-5.jpg") }}" alt=""></a></figure>
                    </div>
                    <div class="info-box">
                        <div class="inner">
                            <h4 class="name"><a href="speakers-detail.html">Kenyetta Lesley</a></h4>
                            <span class="designation">Event Manager</span>
                            <ul class="social-links social-icon-colored">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Speaker Block -->
            <div class="speaker-block col-lg-3 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <figure class="image"><a href="speakers-detail.html"><img
                                    src="{{ asset("website/images/resource/speaker-6.jpg") }}" alt=""></a></figure>
                    </div>
                    <div class="info-box">
                        <div class="inner">
                            <h4 class="name"><a href="speakers-detail.html">Shelly Verghese</a></h4>
                            <span class="designation">Event Manager</span>
                            <ul class="social-links social-icon-colored">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Speaker Block -->
            <div class="speaker-block col-lg-3 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <figure class="image"><a href="speakers-detail.html"><img
                                    src="{{ asset("website/images/resource/speaker-7.jpg") }}" alt=""></a></figure>
                    </div>
                    <div class="info-box">
                        <div class="inner">
                            <h4 class="name"><a href="speakers-detail.html">Cassandra Kopka</a></h4>
                            <span class="designation">Event Manager</span>
                            <ul class="social-links social-icon-colored">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Speaker Block -->
            <div class="speaker-block col-lg-3 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="image-box">
                        <figure class="image"><a href="speakers-detail.html"><img
                                    src="{{ asset("website/images/resource/speaker-8.jpg") }}" alt=""></a></figure>
                    </div>
                    <div class="info-box">
                        <div class="inner">
                            <h4 class="name"><a href="speakers-detail.html">Eugene Clumpner</a></h4>
                            <span class="designation">Event Manager</span>
                            <ul class="social-links social-icon-colored">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
