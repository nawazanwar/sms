<section class="about-section">
    <div class="anim-icons full-width">
        <span class="icon icon-circle-blue wow fadeIn"></span>
        <span class="icon icon-dots wow fadeInleft"></span>
        <span class="icon icon-circle-1 wow zoomIn"></span>
    </div>
    <div class="auto-container">
        <div class="row">
            <!-- Content Column -->
            <div class="content-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column">
                    <div class="sec-title">
                        <span class="title">ABOUT EVENT</span>
                        <h2>Welcome to the World Digital Conference 2020</h2>
                        <div class="text">Dolor sit amet consectetur elit sed do eiusmod tempor incd idunt labore et
                            dolore magna aliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi
                            ut aliquip exea commodo consequat.
                        </div>
                    </div>
                    <ul class="list-style-one">
                        <li>Multiple Announcements during the event.</li>
                        <li>Logo & company details on the WordCamp.</li>
                        <li>Dedicated blog post thanking each Gold.</li>
                        <li>Acknowledgment and opening and closing.</li>
                    </ul>
                    <div class="btn-box"><a href="contact.html" class="theme-btn btn-style-three"><span
                                class="btn-title">Register Now</span></a></div>
                </div>
            </div>

            <!-- Image Column -->
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
                <div class="image-box">
                    <figure class="image wow fadeIn"><img src="/website/images/resource/about-img-1.jpg" alt=""></figure>
                </div>
            </div>
        </div>
    </div>
</section>

