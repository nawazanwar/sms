<section class="bill-banner-section">
    <div class="banner-carousel owl-carousel owl-theme">
        <div class="slide-item" style="background-image: url({{ asset('website/images/main-slider/1.jpg') }});">
            <div class="auto-container">
                <div class="content-box">
                    <span class="title">January 20, 2020</span>
                    <h2> World Digital <br>Conference 2020</h2>
                    <ul class="info-list">
                        <li><span class="icon fa fa-chair"></span> 5000 Seats</li>
                        <li><span class="icon fa fa-user-alt"></span> 12 SPEAKERS</li>
                        <li><span class="icon fa fa-map-marker-alt"></span> Palo, California</li>
                    </ul>
                    <div class="btn-box btn-box"><a href="buy-ticket.html" class="theme-btn btn-style-two"><span
                                class="btn-title">Book Now</span></a></div>
                </div>
            </div>
        </div>
        <div class="slide-item" style="background-image: url({{asset('website/images/main-slider/2.jpg')}});">
            <div class="auto-container">
                <div class="content-box">
                    <span class="title">January 20, 2020</span>
                    <h2> World Digital <br>Conference 2020</h2>
                    <ul class="info-list">
                        <li><span class="icon fa fa-chair"></span> 5000 Seats</li>
                        <li><span class="icon fa fa-user-alt"></span> 12 SPEAKERS</li>
                        <li><span class="icon fa fa-map-marker-alt"></span> Palo, California</li>
                    </ul>
                    <div class="btn-box btn-box"><a href="buy-ticket.html" class="theme-btn btn-style-two"><span
                                class="btn-title">Book Now</span></a></div>
                </div>
            </div>
        </div>
    </div>
</section>
