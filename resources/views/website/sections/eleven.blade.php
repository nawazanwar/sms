<section class="bil-clients-section">
    <div class="anim-icons">
        <span class="icon icon-dots-3 wow zoomIn"></span>
        <span class="icon icon-circle-blue wow zoomIn"></span>
    </div>
    <div class="auto-container">
        <div class="sec-title">
            <span class="title">Clients</span>
            <h2>Offcial Sponsors</h2>
        </div>

        <div class="sponsors-outer">
            <h3>Platinum Sponsors</h3>
            <div class="row">
                <!-- Client Block -->
                <div class="client-block col-lg-3 col-md-6 col-sm-12">
                    <figure class="image-box"><a href="#"><img src="/website/images/clients/1.png" alt=""></a></figure>
                </div>

                <!-- Client Block -->
                <div class="client-block col-lg-3 col-md-6 col-sm-12">
                    <figure class="image-box"><a href="#"><img src="/website/images/clients/2.png" alt=""></a></figure>
                </div>

                <!-- Client Block -->
                <div class="client-block col-lg-3 col-md-6 col-sm-12">
                    <figure class="image-box"><a href="#"><img src="/website/images/clients/3.png" alt=""></a></figure>
                </div>

                <!-- Client Block -->
                <div class="client-block col-lg-3 col-md-6 col-sm-12">
                    <figure class="image-box"><a href="#"><img src="/website/images/clients/4.png" alt=""></a></figure>
                </div>
            </div>
        </div>

        <div class="sponsors-outer">
            <h3>Gold Sponsors</h3>

            <div class="row">
                <!-- Client Block -->
                <div class="client-block col-lg-3 col-md-6 col-sm-12">
                    <figure class="image-box"><a href="#"><img src="/website/images/clients/5.png" alt=""></a></figure>
                </div>

                <!-- Client Block -->
                <div class="client-block col-lg-3 col-md-6 col-sm-12">
                    <figure class="image-box"><a href="#"><img src="/website/images/clients/6.png" alt=""></a></figure>
                </div>

                <!-- Client Block -->
                <div class="client-block col-lg-3 col-md-6 col-sm-12">
                    <figure class="image-box"><a href="#"><img src="/website/images/clients/7.png" alt=""></a></figure>
                </div>

                <!-- Client Block -->
                <div class="client-block col-lg-3 col-md-6 col-sm-12">
                    <figure class="image-box"><a href="#"><img src="/website/images/clients/8.png" alt=""></a></figure>
                </div>
            </div>
        </div>

        <div class="sponsors-outer">
            <h3>Silver Sponsors</h3>

            <div class="row">
                <!-- Client Block -->
                <div class="client-block col-lg-3 col-md-6 col-sm-12">
                    <figure class="image-box"><a href="#"><img src="/website/images/clients/9.png" alt=""></a></figure>
                </div>

                <!-- Client Block -->
                <div class="client-block col-lg-3 col-md-6 col-sm-12">
                    <figure class="image-box"><a href="#"><img src="/website/images/clients/10.png" alt=""></a></figure>
                </div>

                <!-- Client Block -->
                <div class="client-block col-lg-3 col-md-6 col-sm-12">
                    <figure class="image-box"><a href="#"><img src="/website/images/clients/11.png" alt=""></a></figure>
                </div>

                <!-- Client Block -->
                <div class="client-block col-lg-3 col-md-6 col-sm-12">
                    <figure class="image-box"><a href="#"><img src="/website/images/clients/12.png" alt=""></a></figure>
                </div>
            </div>
        </div>
    </div>
</section>
