@extends('layouts.website')
@section('pageTitle', $pageTitle)
@section('content')

    {{-- Section One--}}
    @include('website.sections.one')

    {{--Section Two--}}
    @include('website.sections.two')

    {{--Section Three--}}
    @include('website.sections.three')

    {{--Section Four--}}
    @include('website.sections.four')

    {{--Section Five--}}
    @include('website.sections.five')

    {{--Section Six--}}
    @include('website.sections.six')

    {{--Section Seven--}}
    @include('website.sections.seven')

    {{--Section Eight--}}
    @include('website.sections.eight')

    {{--Section Nine--}}
    @include('website.sections.nine')

    {{--Section Ten--}}
    @include('website.sections.ten')

    {{--Section Eleven--}}
    @include('website.sections.eleven')

    {{--Section Twelve--}}
    @include('website.sections.twelve')

    {{--Section Thirteen--}}
    @include('website.sections.thirteen')

    {{--Section Fourteen--}}
    @include('website.sections.fourteen')

@endsection
