@extends('layouts.website')

@section('content')
    <section class="page-title" style="background-image:url({{asset('website/images/background/6.jpg')}});">
        <div class="auto-container">
            <h1>Privacy Policy</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="index.html">Home</a></li>
                <li>Contact Us</li>
            </ul>
        </div>
    </section>
@endsection
