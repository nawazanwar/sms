@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="card card-solid">
        @can('read',\App\Models\User::class)
            <div class="card-header">
                <div class="card-title">
                    {!! link_to_route('system.users',__('system.all_users'),null,['class'=>'btn btn-info btn-sm']) !!}
                </div>
            </div>
        @endcan
        <div class="card-body pb-0">
            <!-- form start -->
            {!! Form::open(['route' => ['system.users.store'], 'method' => 'POST','files' => true]) !!}
            {!! csrf_field() !!}
            @include('partials.dashboard.message')
            <div class="form-group">
                {!! Form::label('inputName', __('system.name')) !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'autofocus','placeholder'=>__('system.placeholder_name'), 'id' => 'inputName' ]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('inputEmail', __('system.email')) !!}
                {!! Form::text('email', null, ['class' => 'form-control', 'id' => 'inputEmail','placeholder'=>__('system.placeholder_email') ]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('inputTempPassword', __('system.temp_pwd')) !!}
                {!! Form::password('password', ['class' => 'form-control', 'id' => 'inputTempPassword','placeholder'=>__('system.placeholder_temp_password') ]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('image', 'Image') !!}
                <div class="input-group">
                    <div class="custom-file">
                        {!! Form::file('image', null, ['class' => 'custom-file-input', 'id' => 'image' ]) !!}
                        <label class="custom-file-label" for="image">Choose Image</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('cover', 'Cover') !!}
                <div class="input-group">
                    <div class="custom-file">
                        {!! Form::file('cover', null, ['class' => 'custom-file-input', 'id' => 'cover' ]) !!}
                        <label class="custom-file-label" for="image">Choose Cover Photo</label>
                    </div>
                </div>
            </div>
            <div class="form-group icheck-success d-inline">
                {!! Form::checkbox('active', 1, true,['id'=>'inputActive']) !!}
                {!! Form::label('inputActive', __('system.is_active')) !!}
            </div>
            <div class="form-group text-right">
                {!! Form::submit(__('system.save'), array('class' => 'btn btn-primary pull-left btn-sm')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
