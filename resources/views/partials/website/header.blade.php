<header class="bil-header">
    <div class="main-box">
        <div class="auto-container clearfix">
            <div class="logo-box">
                <div class="logo"><a href="index.html"><img src="{{ asset('website/images/logo.png') }}" alt=""
                                                            title=""></a></div>
            </div>

            <!--Nav Box-->
            <div class="nav-outer clearfix">
                <!--Mobile Navigation Toggler-->
                <div class="mobile-nav-toggler"><span class="icon flaticon-menu"></span></div>
                <!-- Main Menu -->
                <nav class="main-menu navbar-expand-md navbar-light">
                    <div class="navbar-header">
                        <!-- Togg le Button -->
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon flaticon-menu-button"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse clearfix" id="navbarSupportedContent">
                        <ul class="navigation clearfix">
                            @foreach($shareNavs as $key=>$value)
                                <li><a href="">{{ $value->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </nav>
                <!-- Main Menu End-->
                <!-- Outer box -->
                <div class="outer-box">
                    <!--Search Box-->
                    <div class="search-box-outer">
                        <div class="search-box-btn"><span class="flaticon-search"></span></div>
                    </div>
                    <!-- Button Box -->
                    @if(Auth::guest())
                        <div class="bill-btn-box">
                            <a href="{{ route('login') }}" class="theme-btn btn-style-one mx-1">
                                <span class="btn-title">Login</span>
                            </a>
                            <a href="{{ route('register') }}" class="theme-btn btn-style-one mx-1">
                                <span class="btn-title">Register</span>
                            </a>
                        </div>
                    @else
                        {{-- <nav class="main-menu navbar-expand-md navbar-light">
                             <li class="dropdown">
                                 <a href="speakers.html">{{ Auth::user()->name }}</a>
                                 <ul>
                                     <li><a href="{{ route('home') }}">Dashboard</a></li>
                                     <li><a href="{{ route('logout') }}">Logout</a></li>
                                 </ul>
                             </li>
                         </nav>--}}
                        <nav class="main-menu navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                    <li class="current dropdown">
                                        <a href="#">{{ Auth::user()->name }}</a>
                                        <ul>
                                            <li><a href="{{ route('logout') }}">Log Out</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div class="btn-box">
                            <a href="{{ route('home') }}" class="theme-btn btn-style-one mx-1">
                                <span class="btn-title">Dashboard</span>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-menu">
        <div class="menu-backdrop"></div>
        <div class="close-btn"><span class="icon flaticon-cancel-1"></span></div>

        <!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header-->
        <nav class="menu-box">
            <div class="nav-logo"><a href="index.html"><img src="{{ asset('website/images/logo-2.png') }}" alt=""
                                                            title=""></a></div>

            <ul class="navigation clearfix"><!--Keep This Empty / Menu will come through Javascript--></ul>
        </nav>
    </div>
</header>
