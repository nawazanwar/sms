<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>@yield('pageTitle')</title>
    <link href="{{asset('website/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{ asset('website/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('website/css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('css/color-switcher-design.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('website/images/favicon.png') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('website/images/favicon.png') }}" type="image/x-icon">
    <link href="{{ asset('website/css/custom.css') }}" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
</head>
<body>
<div class="page-wrapper">
    <section class="banner-conference-two auth-holder"
             style="background-image: url({{ asset('website/images/background/6.jpg') }})">
        @yield('content')
    </section>
</div>
</body>
