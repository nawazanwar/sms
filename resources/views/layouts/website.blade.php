<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>@yield('pageTitle')</title>
    <link href="{{asset('website/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{ asset('website/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('website/css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('css/color-switcher-design.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('website/images/favicon.png') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('website/images/favicon.png') }}" type="image/x-icon">

    <link rel="stylesheet" href="{{ asset('website/css/custom.css') }}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
</head>
<body>
<div class="page-wrapper">
    <div class="preloader"></div>
    @include('partials.website.header')
    <main>
        @yield('content')
    </main>
    @include('partials.website.footer')
</div>
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div>
<script src="{{ asset('website/js/jquery.js') }}"></script>
<script src="{{ asset('website/js/popper.min.js') }}"></script>
<script src="{{ asset('website/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('website/js/jquery-ui.js') }}"></script>
<script src="{{ asset('website/js/jquery.fancybox.js') }}"></script>
<script src="{{ asset('website/js/appear.js') }}"></script>
<script src="{{ asset('website/js/owl.js') }}"></script>
<script src="{{ asset('website/js/jquery.countdown.js') }}"></script>
<script src="{{ asset('website/js/wow.js') }}"></script>
<script src="{{ asset('website/js/script.js') }}"></script>
<script src="{{ asset('website/js/color-settings.js') }}"></script>
</body>
