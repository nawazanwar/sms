<?php

return [

    'driver' => 'gd',
    'size' => [
        'large' => 720,
        'medium' => 458,
        'small' => 360,
        'tiny' => 80
    ],
    'author_avatar_size' => 70,
];
