<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Course extends Model implements Permissions
{
    protected $fillable = ['name', 'description', 'code'];

    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }

    /*Belongs to many users*/
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /*Belongs to many grades*/
    public function grades()
    {
        return $this->belongsToMany(Grade::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_course');
                    break;
                case 'create':
                case 'store':
                    return array('create_course');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_course');
                    break;
                case 'delete':
                    return array('delete_course');
                    break;
                default:
                    return array();
            }
        }

        return array(
            'read_course',
            'create_course',
            'edit_course',
            'delete_course',
        );
    }
}
