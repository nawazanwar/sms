<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    protected $fillable = ['start_data', 'current'];

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function roles()
    {
        return $this->belongsTo(Role::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_salary');
                    break;
                case 'create':
                case 'store':
                    return array('create_salary');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_salary');
                    break;
                case 'delete':
                    return array('delete_salary');
                    break;
                default:
                    return array();
            }
        }

        return array(
            'read_salary',
            'create_salary',
            'edit_salary',
            'delete_salary',
        );
    }
}
