<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Grade extends Model implements Permissions
{
    use SoftDeletes;
    protected $fillable = ['name', 'label'];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    // many grade have many courses
    public function courses(){
        return $this->belongsToMany(Course::class);
    }
    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_grade');
                    break;
                case 'create':
                case 'store':
                    return array('create_grade');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_grade');
                    break;
                case 'delete':
                    return array('delete_grade');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_grade',
            'create_grade',
            'edit_grade',
            'delete_grade',
        );
    }
}
