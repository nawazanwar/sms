<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class Profile extends Model implements Permissions
{
    protected $fillable = ['gender', 'date_of_birth', 'temporary_address','permanent_address','country','state','city','mobile','phone','zip_code'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_profile');
                    break;
                case 'create':
                case 'store':
                    return array('create_profile');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_profile');
                    break;
                case 'delete':
                    return array('delete_profile');
                    break;
                default:
                    return array();
            }
        }

        return array(
            'read_profile',
            'create_profile',
            'edit_profile',
            'delete_profile',
        );
    }
}
