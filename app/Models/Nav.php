<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class Nav extends Model implements Permissions
{
    protected $fillable = ['name', 'url'];
    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_nav');
                    break;
                case 'create':
                case 'store':
                    return array('create_nav');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_nav');
                    break;
                case 'delete':
                    return array('delete_nav');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_nav',
            'create_nav',
            'edit_nav',
            'delete_nav',
        );
    }
}
