<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class Institution extends Model implements Permissions
{
    protected $table = 'institutions';
    protected $fillable = ['name', 'address', 'email','phone','mobile','fax','contact_person'];

    //Many branches (one-to-many relationship)
    public function branches()
    {
        return $this->hasMany(InstitutionBranch::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_institution');
                    break;
                case 'create':
                case 'store':
                    return array('create_institution');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_institution');
                    break;
                case 'delete':
                    return array('delete_institution');
                    break;
                default:
                    return array();
            }
        }

        return array(
            'read_institution',
            'create_institution',
            'edit_institution',
            'delete_institution',
        );
    }
}
