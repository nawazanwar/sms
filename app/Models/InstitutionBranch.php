<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class InstitutionBranch extends Model implements Permissions
{
    protected $table = 'institutionbranches';
    protected $fillable = ['institution_id', 'location'];

    public function institution()
    {
        return $this->belongsTo(Institution::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_institution_branch');
                    break;
                case 'create':
                case 'store':
                    return array('create_institution_branch');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_institution_branch');
                    break;
                case 'delete':
                    return array('delete_institution_branch');
                    break;
                default:
                    return array();
            }
        }

        return array(
            'read_institution_branch',
            'create_institution_branch',
            'edit_institution_branch',
            'delete_institution_branch',
        );
    }
}
