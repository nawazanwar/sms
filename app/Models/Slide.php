<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;

class Slide extends Model implements Permissions
{
    protected $fillable = ['first_heading', 'second_heading', 'image'];

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_slide');
                    break;
                case 'create':
                case 'store':
                    return array('create_slide');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_slide');
                    break;
                case 'delete':
                    return array('delete_slide');
                    break;
                default:
                    return array();
            }
        }

        return array(
            'read_slide',
            'create_slide',
            'edit_slide',
            'delete_slide',
        );
    }
}
