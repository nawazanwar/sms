<?php

namespace App\Providers;

use App\Models\Grade;
use App\Models\Institution;
use App\Models\Nav;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Setting;
use App\Models\Slide;
use App\Models\User;
use App\Policies\GradePolicy;
use App\Policies\InstitutionBranchesPolicy;
use App\Policies\InstitutionPolicy;
use App\Policies\NavPolicy;
use App\Policies\PermissionPolicy;
use App\Policies\RolePolicy;
use App\Policies\SettingPolicy;
use App\Policies\SlidePolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Permission::class => PermissionPolicy::class,
        Role::class => RolePolicy::class,
        User::class => UserPolicy::class,
        Grade::class => GradePolicy::class,
        Setting::class => SettingPolicy::class,
        Nav::class => NavPolicy::class,
        Slide::class => SlidePolicy::class,
        Institution::class => InstitutionPolicy::class,
        InstitutionBranchesPolicy::class => InstitutionBranchesPolicy::class

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
