<?php

namespace App\Providers;

use App\Models\Nav;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasTable('navs')) {
            $navs = Nav::all();
        } else {
            $navs = [];
        }
        view()->share('shareNavs', $navs);
    }
}
