<?php

namespace App\Http\Controllers\Academic;

use App\Models\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Grade;
use App\Models\Permission;

class GradeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = new Grade();
        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        if ($field && $keyword) {
            $data = $data->where('grades.' . $field, 'like', '%' . $keyword . '%');
        }
        $data = $data->orderby('grades.id', 'DESC')->paginate(10);
        $pageTitle = __('academic.all_grades');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'field' => $field,
            'keyword' => $keyword,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $data,
        ];
        return view('academic.grades.index', $viewParams);
    }

    public function create()
    {

        $pageTitle = __('system.create_role');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
        ];

        return view('system.roles.create', $viewParams);
    }

    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'name' => 'required|max:255|unique:roles,name',
            'label' => 'required|max:255',
        ]);
        if (Role::create($validatedData)) {
            return redirect()->route('system.roles')->with('successMessage', __('system.role_created_success_message'));
        } else {
            return redirect()->back()
                ->withErrors(__('system.role_created_failed_message'))
                ->withInput();
        }
    }

    public function edit($id)
    {
        $model = Role::find($id);
        $pageTitle = __('system.edit') . " " . $model->label;
        $breadcrumbs = [['text' => __('system.edit_role')]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];

        return view('system.roles.edit', $viewParams);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:4|unique:roles,name,' . $id,
            'label' => 'required|max:255',
        ]);

        if (Role::whereId($id)->update($validatedData)) {
            return redirect()->route('system.roles')->with('successMessage', $validatedData['label'] . " " . __('system.updated_success_message'));
        } else {
            return redirect()->back()
                ->withErrors($validatedData['label'] . " " . __('system.updated_failed_message_message'))
                ->withInput();
        }
    }

    public function destroy($id)
    {
        $model = Role::find($id);
        if ($model->delete()) {
            return redirect()->route('system.permissions')->with('errorMessage', $model->label . " " . __('system.deleted_successfully'));
        }
    }

    public function permissions($id, Request $request)
    {
        $model = Role::find($id);
        $permissions = Permission::all();
        $pageTitle = __('system.all_permissions_of') . " " . $model->label;
        $breadcrumbs = [['text' => __('system.all_permissions')]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'permissions' => $permissions
        ];
        return view('system.roles.permissions', $viewParams);
    }

    public function users($id, Request $request)
    {
        $model = Role::find($id);
        $users = User::all();
        $pageTitle = __('system.all_users_of') . " " . $model->name;
        $breadcrumbs = [['text' => __('system.all_users')]];
        $viewParams = [
            'model' => $model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'users' => $users
        ];
        return view('system.roles.users', $viewParams);
    }


    public function assignPermission($rId, $pId, $type)
    {
        $model = Role::find($rId);
        $permission = Permission::find($pId);
        if ($type == 'deActive') {
            $model->permissions()->detach($pId);
        } else {
            $model->permissions()->save($permission);
        }
        return response()->json(['type' => $type, 'label' => $permission->label]);
    }

    public function assignUser($rId, $uId, $type)
    {
        $model = Role::find($rId);
        $user = User::find($uId);
        if ($type == 'deActive') {
            $model->users()->detach($uId);
        } else {
            $model->users()->save($user);
        }
        return response()->json(['type' => $type, 'label' => $user->name]);
    }
}
