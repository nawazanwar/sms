<?php

namespace App\Http\Controllers\System;

use App\Models\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Slide;
use App\Models\Permission;

class SlideController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = new Slide();
        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        if ($field && $keyword) {
            $data = $data->where('slides.' . $field, 'like', '%' . $keyword . '%');
        }
        $data = $data->orderby('slides.id', 'DESC')->paginate(10);
        $pageTitle = __('system.all_slides');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'field' => $field,
            'keyword' => $keyword,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $data,
        ];
        return view('system.slides.index', $viewParams);
    }

}
