<?php

namespace App\Http\Controllers\System;

use App\Models\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Nav;
use App\Models\Permission;

class NavController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = new Nav();
        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        if ($field && $keyword) {
            $data = $data->where('navs.' . $field, 'like', '%' . $keyword . '%');
        }
        $data = $data->orderby('navs.id', 'DESC')->paginate(10);
        $pageTitle = __('system.all_navs');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'field' => $field,
            'keyword' => $keyword,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $data,
        ];
        return view('system.navs.index', $viewParams);
    }

}
