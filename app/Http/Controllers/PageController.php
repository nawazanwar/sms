<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function contact_us(Request $request)
    {
       return view('pages.contact-us');
    }
    public function  privacy_policy(Request $request){
        return view('pages.privacy-policy');
    }
    public function terms_and_conditions(Request $request){
        return view('pages.terms-and-conditions');
    }
}
