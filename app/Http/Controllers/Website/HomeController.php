<?php
namespace App\Http\Controllers\Website;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $pageTitle = __('website.welcome');
        $viewParams = [
            'pageTitle' => $pageTitle,
        ];
        return view('website.home', $viewParams);
    }
}
