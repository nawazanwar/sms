<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class NavPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_nav');
    }

    public function create(User $user)
    {
        return $user->ability('create_nav');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_nav');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_nav');
    }
}
