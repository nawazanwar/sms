<?php


namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class SlidePolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_slide');
    }

    public function create(User $user)
    {
        return $user->ability('create_slide');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_slide');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_slide');
    }
}
