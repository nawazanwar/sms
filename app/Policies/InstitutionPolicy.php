<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class InstitutionPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_institution');
    }

    public function create(User $user)
    {
        return $user->ability('create_institution');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_institution');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_institution');
    }
}
