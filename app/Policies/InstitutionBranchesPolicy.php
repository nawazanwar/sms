<?php


namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class InstitutionBranchesPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_institution_branch');
    }

    public function create(User $user)
    {
        return $user->ability('create_institution_branch');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_institution_branch');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_institution_branch');
    }
}
