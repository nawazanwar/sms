<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class GradePolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_grade');
    }

    public function create(User $user)
    {
        return $user->ability('create_grade');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_grade');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_grade');
    }
}
